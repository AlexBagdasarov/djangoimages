import re, urllib.request, ssl, datetime
from django.shortcuts import render
from django.http import HttpResponse
from .forms import UrlForm
from DjangoImages.ImageDetection.ImageSearch.DirectoryGenerating import DirGen
from DjangoImages.ImageDetection.ImageSearch.Poster import Poster
from DjangoImages.ImageDetection.ImageSearch.Crawler import Crawler

# Create your views here.

gendir = DirGen()
poster = Poster()
crawler = Crawler()

datedirname = datetime.datetime.now().strftime("%Y-%m-%d")

def index(request):
    #return HttpResponse("<center><h2>Hello World!</h2></center>")
    form = UrlForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        print(form.cleaned_data)
        context = ssl._create_unverified_context()
        #url = "http://lenta.ru/"
        url = form.cleaned_data["surl"]
        #Check date dir
        datedir = gendir.create(datedirname)
        #Check url dir
        urldir = gendir.create(datedirname+'/'+url)
        total = crawler.crawl(urldir, url)
    return render(request, 'imagesearch/index.html', locals())
