import os

class DirGen:
    def create(self, dirname):
        path = ('./files/'+dirname)
        try:
            os.makedirs(path)
        except OSError:
            return 'check'
        return dirname
