import re, urllib.request, ssl

class Crawler:
    def crawl(self, dir, url):
        context = ssl._create_unverified_context()
        content = urllib.request.urlopen(url, context=context).read().decode('utf-8')
        imgUrls = re.findall('img .*?src="(.*?)"', content)
        count = 1
        for img in imgUrls:
            url = img
            img = urllib.request.urlopen(url, context=context).read()
            out = open("./files/"+dir+"/{0}.jpg".format(count), "wb")
            out.write(img)
            out.close
            count += 1
        total = count
        return  total