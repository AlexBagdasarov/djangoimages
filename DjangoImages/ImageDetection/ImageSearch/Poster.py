import requests
import os
import datetime

class Poster:
    def post(self):
        dirname = datetime.datetime.now().strftime("%Y-%m-%d")
        url = "http://172.19.12.51:9080/powerai-vision/api/dlapis/"+"774ffdce-843c-4133-8e15-5f78922519d0"
        path = './files/'+dirname+'/'

        payload = {
            'Content-Disposition': 'form-data',
            'name': 'files',
        }
        headers = {
            'content-type': "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
            'Cache-Control': "no-cache",
            'Postman-Token': "79772d47-f3fc-4077-9dba-17fc9d53f031"
            }

        for filename in os.listdir(path):
            files = {
                'files': open(path+filename, 'rb')
            }
            response = requests.post(url, files=files)
            data = response.json()
            print (data['classified'])


# def main():
#     filename = './files/1.jpg'
#     url = "http://172.19.12.51:9080/powerai-vision/api/dlapis/"+"94b8e164-ef2a-4514-8b15-4c3507bcdd28"
#     c = pycurl.Curl
#     c.setopt(pycurl.VERBOSE, 1)
#     c.setopt(pycurl.URL, url)
#     fout = io.StringIO()
#     c.setopt(pycurl.WRITEFUNCTION, fout.write)
#     c.setopt(pycurl.POST, 1)
#     c.setopt(pycurl.HTTPHEADER, [
#         'Content-Type: image/jpeg'])
#
#     filesize = os.path.getsize(filename)
#     c.setopt(pycurl.POSTFIELDSIZE, filesize)
#     fin = open(filename, 'rb')
#     c.setopt(pycurl.READFUNCTION, fin.read)
#     c.perform()
#
#     response_code = c.getinfo(pycurl.RESPONSE_CODE)
#     response_data = fout.getvalue()
#     print (response_code)
#     print (response_data)
#     c.close()
