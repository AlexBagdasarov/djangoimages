from django.db import models

# Create your models here.
class UrlSender(models.Model):
    surl = models.URLField()

    def __str__(self):
        return "%s" % (self.surl)

class CrawlResult(models.Model):
    crawlres = models.IntegerField()