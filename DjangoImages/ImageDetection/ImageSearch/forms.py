from django import forms
from .models import *

class UrlForm(forms.ModelForm):

    class Meta:
        model = UrlSender
        exclude = [""]

class CrawlForm(forms.ModelForm):
    class Meta:
        model = CrawlResult
        exclude = [""]